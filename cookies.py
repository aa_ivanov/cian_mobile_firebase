import json
import time
import os

import requests
from selenium import webdriver
from selenium.webdriver import ChromeOptions

url = 'https://console.firebase.google.com/u/0/project/cian-e96d7/crashlytics/app/android:ru.cian.main/issues?state=open&time=last-seven-days&type=crash/'
login = os.environ['FIREBASE_LOGIN']
password = os.environ['FIREBASE_PASSWORD']
chrome_options = ChromeOptions()
chrome_options.add_argument('disable-notifications')
chrome_options.add_argument('process-per-site')
chrome_options.add_argument('dns-prefetch-disable')
chrome_options.add_argument('--ignore-certificate-errors')
chrome_options.add_experimental_option('w3c', False)
desired_capabilities = chrome_options.to_capabilities()
desired_capabilities['version'] = '94.0'
driver = webdriver.Remote(command_executor='http://test:test@192.168.16.156:4444/wd/hub',
                          desired_capabilities=desired_capabilities)
cookie_list = ['SID', 'HSID', 'SSID', 'APISID', 'SAPISID', 'NID', 'SIDCC', '__Secure-3PSIDCC', '__Secure-3PSID', '__Secure-3PAPISID']

driver.get(url)
login_field = driver.find_element_by_name('identifier')
login_field.send_keys(login)
next_btn = driver.find_element_by_xpath('//*[@id="identifierNext"]')
next_btn.click()
time.sleep(5)
password_field = driver.find_element_by_name('password')
password_field.send_keys(password)
next_btn2 = driver.find_element_by_xpath('//*[@id="passwordNext"]')
next_btn2.click()
time.sleep(10)
cookie_dict = {}

for cookie in cookie_list:
    cookie_dict[cookie] = driver.get_cookie(cookie)['value']

cookie_dict = {'cookies': cookie_dict}

print(cookie_dict)

requests.post('http://mobile-flow-helper.micro.cian.tech/v1/set-firebase-settings/', data=json.dumps(cookie_dict))

driver.quit()
